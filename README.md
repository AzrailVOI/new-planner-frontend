# Front-end application for [Azraїl PLANNER](https://planner.azrail.xyz)

## Requirements

- [Node.js](https://nodejs.org/en/) version 16 or higher
- [pnpm](https://pnpm.io/)

## Technologies

- [Typescript](https://www.typescriptlang.org/)
- [NextJS](https://nextjs.org/)
- [TailwindCSS](https://tailwindcss.com/)
- [React Query](https://react-query.tanstack.com/)

## Installation

```bash
pnpm install
```

## You must create .env file like:

```
NEXT_PUBLIC_HOST='localhost'
NEXT_PUBLIC_SERVER='http://localhost:4200'
```

## Getting Started

Development:
```bash
pnpm dev
```
Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

Production:
```bash
pnpm build
pnpm start
```

## License

[MIT License](LICENSE)
