FROM node
LABEL authors="Vladyslav Ovčaruk"

WORKDIR /app
COPY . /app
RUN npm install -g pnpm
RUN pnpm install
RUN pnpm build
CMD ["pnpm", "start"]

EXPOSE 3000
