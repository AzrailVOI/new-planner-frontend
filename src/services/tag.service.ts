import { ITagResponse, TypeTagFormState } from '@/types/tag.types'

import { axiosWithAuth } from '@/api/interseptors'

class TagService {
	private BASE_URL = '/user/tag'

	async getTags() {
		const response = await axiosWithAuth.get<ITagResponse[]>(this.BASE_URL)

		return response
	}

	async getTagByName(name: string) {
		const response = await axiosWithAuth.get<ITagResponse[]>(
			`${this.BASE_URL}/${name}`
		)

		return response
	}

	async createTag(data: TypeTagFormState) {
		const response = await axiosWithAuth.post<ITagResponse[]>(
			this.BASE_URL,
			data
		)

		return response
	}

	async updateTag(id: string, data: TypeTagFormState) {
		const response = await axiosWithAuth.put(`${this.BASE_URL}/${id}`, data)

		return response
	}

	async deleteTag(id: string) {
		const response = await axiosWithAuth.delete(`${this.BASE_URL}/${id}`)
		return response
	}
}

export const tagService = new TagService()
