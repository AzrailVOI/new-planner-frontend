import {
	ITagToTaskResponse,
	TypeTagToTaskFormState
} from '@/types/tag-to-task.types'

import { axiosWithAuth } from '@/api/interseptors'

class TagToTaskService {
	private BASE_URL = '/user/tag-to-task'

	async getTagsToTaskByTag(taskId: string) {
		const response = await axiosWithAuth.get<ITagToTaskResponse[]>(
			`${this.BASE_URL}/${taskId}`
		)
		return response
	}

	async getTagsToTask() {
		const response = await axiosWithAuth.get<Array<ITagToTaskResponse>>(
			this.BASE_URL
		)
		return response
	}

	async createTagToTask(data: TypeTagToTaskFormState) {
		const response = await axiosWithAuth.post(this.BASE_URL, data)

		return response
	}

	//tagId=at2sej4qfvk45jrcpyxtj25c&taskId=jynppmew0nig4axsodgwu3cc
	async deleteTagToTask({ taskId, tagId }: TypeTagToTaskFormState) {
		const response = await axiosWithAuth.delete(`${this.BASE_URL}`, {
			params: { tagId, taskId }
		})
		return response
	}
}

export const tagToTaskService = new TagToTaskService()
