import {
	ITaskColumnResponse,
	TypeTaskColumnFormState
} from '@/types/task-column.types'

import { axiosWithAuth } from '@/api/interseptors'

class TaskColumnService {
	private BASE_URL = '/user/task-column'

	async getTaskColumns() {
		const response = await axiosWithAuth.get<ITaskColumnResponse[]>(
			this.BASE_URL
		)
		return response
	}

	async createTaskColumn(data: TypeTaskColumnFormState) {
		const response = await axiosWithAuth.post(this.BASE_URL, data)

		return response
	}

	async updateTaskColumn(id: string, data: TypeTaskColumnFormState) {
		const response = await axiosWithAuth.put(`${this.BASE_URL}/${id}`, data)

		return response
	}

	async updateOrder(ids: Array<string>) {
		const response = await axiosWithAuth.put(`${this.BASE_URL}/update-order`, {
			ids
		})
		return response
	}

	async deleteTaskColumn(id: string) {
		const response = await axiosWithAuth.delete(`${this.BASE_URL}/${id}`)
		return response
	}
}

export const taskColumnService = new TaskColumnService()
