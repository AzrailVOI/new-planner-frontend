import { KeyboardEvent } from 'react'

export function useClick(key: string) {
	const handleClick = (
		event: KeyboardEvent<HTMLElement>,
		callback: () => void
	) => {
		if (event.key === key) {
			event.preventDefault()
			event.stopPropagation()
			callback()
		}
	}

	return { handleClick }
}
