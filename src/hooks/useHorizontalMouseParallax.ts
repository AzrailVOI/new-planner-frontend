import { RefObject, useEffect, useRef } from 'react'

interface ParallaxHook {
	ref: RefObject<HTMLDivElement>
}

export function useHorizontalMouseParallax(): ParallaxHook {
	const isMouseDown = useRef<boolean>(false)
	const mouseX = useRef<number>(0)
	const ref = useRef<HTMLDivElement>(null)

	const handleMouseMove = (e: MouseEvent) => {
		if (!ref.current) return
		const deltaX = e.clientX - mouseX.current
		const speed = 3 // Увеличьте значение, чтобы увеличить скорость прокрутки
		ref.current.scrollLeft += deltaX * speed
		mouseX.current = e.clientX
		// console.log(
		// 	'move',
		// 	mouseX,
		// 	e.clientX,
		// 	'deltaX',
		// 	deltaX,
		// 	ref.current.scrollLeft
		// )
	}

	const handleMouseDown = (e: MouseEvent) => {
		isMouseDown.current = true
		mouseX.current = e.clientX
	}

	const handleMouseUpLeave = (e: MouseEvent) => {
		if (!ref.current) return
		isMouseDown.current = false
		mouseX.current = e.clientX
		// mouseX.current = 0
		// ref.current.scrollLeft = 0
	}

	useEffect(() => {
		if (!ref.current) return

		ref.current.addEventListener('mousedown', handleMouseDown)
		ref.current.addEventListener('mouseup', handleMouseUpLeave)
		ref.current.addEventListener('mouseleave', handleMouseUpLeave)
		ref.current.addEventListener('mousemove', handleMouseMove)

		return () => {
			if (!ref.current) return

			ref.current.removeEventListener('mousedown', handleMouseDown)
			ref.current.removeEventListener('mouseup', handleMouseUpLeave)
			ref.current.removeEventListener('mouseleave', handleMouseUpLeave)
			ref.current.removeEventListener('mousemove', handleMouseMove)
		}
	}, [])

	return { ref }
}
