import { ITagResponse } from '@/types/tag.types'
import type { ITaskResponse } from '@/types/task.types'

export const filterTasks = (
	tasks: ITaskResponse[] | undefined,
	value: string,
	selectedTags: Array<ITagResponse>
) => {
	return tasks?.filter(task => {
		const tagsIds = task.tags?.map((tag: ITagResponse) => tag.id)
		if (selectedTags.length === 0) {
			return task.columnId === value
		} else {
			return (
				task.columnId === value &&
				selectedTags.every(tag => tagsIds?.includes(tag.id))
			)
		}
	})
}
