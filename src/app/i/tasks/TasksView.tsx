'use client'

import clsx from 'clsx'

import Loader from '@/components/ui/Loader'

import { ITagResponse } from '@/types/tag.types'

import { useLocalStorage } from '@/hooks/useLocalStorage'

import SwitcherView from '@/app/i/tasks/SwitcherView'
import KanbanView from '@/app/i/tasks/kanban-view/KanbanView'
import ListView from '@/app/i/tasks/list-view/ListView'
import Tag from '@/app/i/tasks/tags/Tag'

interface ITasksView {}
export type TypeView = 'list' | 'kanban'

export default function TasksView({}: ITasksView) {
	const [type, setType, isLoading] = useLocalStorage<TypeView>({
		key: 'view-type',
		defaultValue: 'list'
	})

	const [selectedTags, setSelectedTags] = useLocalStorage<Array<ITagResponse>>({
		key: 'selected-tags',
		defaultValue: []
	})

	if (isLoading) return <Loader />

	return (
		<div>
			<SwitcherView
				type={type}
				setType={setType}
			/>
			<div className={'w-full mt-4 mb-4 flex justify-start items-center'}>
				<h2 className={clsx('text-xl')}>
					{selectedTags.length > 0 ? 'Selected tags: ' : 'No tags selected'}
				</h2>
				{selectedTags.map((tag, index) => (
					<div className={'ml-1.5'}>
						<Tag
							key={index}
							itemId={''}
							tagItem={tag}
							setSelectedTags={setSelectedTags}
							selectedTags={selectedTags}
						/>
					</div>
				))}
			</div>

			{type === 'list' ? (
				<ListView
					selectedTags={selectedTags}
					setSelectedTags={setSelectedTags}
				/>
			) : (
				<KanbanView
					setSelectedTags={setSelectedTags}
					selectedTags={selectedTags}
				/>
			)}
		</div>
	)
}
