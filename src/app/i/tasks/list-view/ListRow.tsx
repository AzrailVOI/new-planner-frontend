import clsx from 'clsx'
import { GripVertical, Trash } from 'lucide-react'
import type { Dispatch, SetStateAction } from 'react'
import { Controller, useForm } from 'react-hook-form'

import Loader from '@/components/ui/Loader'
import Checkbox from '@/components/ui/checkbox'
import { TransparentField } from '@/components/ui/fields/TransparentField'
import SingleSelect from '@/components/ui/task-edit/SingleSelect'

import { ITagResponse } from '@/types/tag.types'
import {
	type ITaskResponse,
	type TypeTaskFormState,
	priorityList
} from '@/types/task.types'

import styles from './ListView.module.scss'
import { useDeleteTask } from '@/app/i/tasks/hooks/tasks/useDeleteTask'
import { useTaskDebounce } from '@/app/i/tasks/hooks/tasks/useTaskDebounce'
import AddTag from '@/app/i/tasks/tags/AddTag'
import Tags from '@/app/i/tasks/tags/Tags'

interface IListRow {
	item: ITaskResponse
	setItems: Dispatch<SetStateAction<ITaskResponse[] | undefined>>

	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function ListRow({
	item,
	setItems,
	setSelectedTags,
	selectedTags
}: IListRow) {
	const { register, control, watch } = useForm<TypeTaskFormState>({
		defaultValues: {
			name: item.name,
			isCompleted: item.isCompleted,
			priority: item.priority,
			columnId: item.columnId,
			description: item.description
		}
	})

	useTaskDebounce({ watch, itemId: item.id })

	const { deleteTask, isDeletePending } = useDeleteTask()

	return (
		<div
			className={clsx(
				styles.row,
				watch('isCompleted') ? styles.completed : '',
				'animation-opacity'
			)}
		>
			<div>
				<span className='inline-flex items-center gap-2.5 w-full'>
					<button aria-describedby='todo-iem'>
						<GripVertical className={styles.grip} />
					</button>

					<Controller
						control={control}
						name='isCompleted'
						render={({ field: { value, onChange } }) => (
							<Checkbox
								onChange={onChange}
								checked={value}
							/>
						)}
					/>

					<TransparentField
						{...register('name')}
						placeholder={'Enter task name'}
					/>
				</span>
			</div>
			<div>
				<TransparentField
					{...register('description')}
					placeholder={'Enter task description'}
				/>
			</div>
			<div className='capitalize'>
				<Controller
					control={control}
					name={'priority'}
					render={({ field: { value, onChange } }) => (
						<SingleSelect
							onChange={onChange}
							value={value || ''}
							data={priorityList.map(item => ({
								value: item,
								label: item
							}))}
						/>
					)}
				/>
			</div>
			{item.tags && (
				<div className={styles.tags_container}>
					<Tags
						selectedTags={selectedTags}
						setSelectedTags={setSelectedTags}
						tags={item.tags}
						itemId={item.id}
					/>
					<AddTag
						taskId={item.id}
						view={'list'}
						usedTags={item.tags.map((tag: ITagResponse) => tag.id)}
					/>
				</div>
			)}
			<div>
				<button
					onClick={() =>
						item.id ? deleteTask(item.id) : setItems(prev => prev?.slice(0, -1))
					}
					className='opacity-50 transition-opacity hover:opacity-100'
				>
					{isDeletePending ? <Loader size={14} /> : <Trash size={14} />}
				</button>
			</div>
		</div>
	)
}
