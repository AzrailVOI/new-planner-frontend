import { Dispatch, SetStateAction } from 'react'

import type { ITaskResponse } from '@/types/task.types'

import styles from './ListView.module.scss'

interface IListAddRowInput {
	columnId: string
	setItems: Dispatch<SetStateAction<ITaskResponse[] | undefined>>
}

export default function ListAddRowInput({
	columnId,
	setItems
}: IListAddRowInput) {
	const addRow = () => {
		setItems(prevState => {
			if (!prevState) return []
			return [
				...prevState,
				{
					id: '',
					name: '',
					isCompleted: false,
					columnId: columnId,
					description: ''
				}
			]
		})
	}

	return (
		<div className={styles.addRow}>
			<button
				onClick={addRow}
				className='italic opacity-40 text-sm'
			>
				Add task...
			</button>
		</div>
	)
}
