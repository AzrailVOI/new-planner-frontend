import { Dispatch, SetStateAction } from 'react'

import { ITaskColumnResponse } from '@/types/task-column.types'

import styles from './ListView.module.scss'

interface IListAddColumn {
	setColumns: Dispatch<SetStateAction<ITaskColumnResponse[] | undefined>>
}

export default function ListAddColumn({ setColumns }: IListAddColumn) {
	const addColumn = () => {
		setColumns(prevState => {
			if (!prevState) return []
			return [
				...prevState,
				{
					id: '',
					name: '',
					order: 99
				}
			]
		})
	}

	return (
		<div className={styles.column}>
			<div className='mt-5'>
				<button
					onClick={addColumn}
					className='italic opacity-40 text-sm text-left font-semibold'
				>
					Add group...
				</button>
			</div>
		</div>
	)
}
