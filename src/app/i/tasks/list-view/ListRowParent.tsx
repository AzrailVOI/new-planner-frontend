import { Draggable, Droppable } from '@hello-pangea/dnd'
import { GripVertical, Trash } from 'lucide-react'
import { Dispatch, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'

import Loader from '@/components/ui/Loader'
import { TransparentField } from '@/components/ui/fields/TransparentField'

import { ITagResponse } from '@/types/tag.types'
import {
	ITaskColumnResponse,
	TypeTaskColumnFormState
} from '@/types/task-column.types'
import { ITaskResponse } from '@/types/task.types'

import styles from './ListView.module.scss'
import { filterTasks } from '@/app/i/tasks/filter-tasks'
import { useDeleteTaskColumn } from '@/app/i/tasks/hooks/columns/useDeleteTaskColumn'
import { useTaskColumnDebounce } from '@/app/i/tasks/hooks/columns/useTaskColumnDebounce'
import { useTaskColumnSortable } from '@/app/i/tasks/hooks/columns/useTaskColumnSortable'
import ListAddRowInput from '@/app/i/tasks/list-view/ListAddRowInput'
import ListRow from '@/app/i/tasks/list-view/ListRow'

interface IListRowParent {
	value: string
	label: string
	items: ITaskResponse[] | undefined
	setColumns: Dispatch<SetStateAction<ITaskColumnResponse[] | undefined>>
	setItems: Dispatch<SetStateAction<ITaskResponse[] | undefined>>

	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function ListRowParent({
	value,
	items,
	setItems,
	setColumns,
	label,
	selectedTags,
	setSelectedTags
}: IListRowParent) {
	const { register, watch } = useForm<TypeTaskColumnFormState>({
		defaultValues: {
			name: label
		}
	})
	useTaskColumnDebounce({ watch, itemId: value })

	const { attributes, style, setNodeRef, listeners } =
		useTaskColumnSortable(value)

	const { deleteTaskColumn, isDeletePending } = useDeleteTaskColumn()
	return (
		<div
			ref={setNodeRef}
			style={style}
		>
			<div className={styles.block}>
				<Droppable droppableId={value}>
					{provided => (
						<div
							ref={provided.innerRef}
							{...provided.droppableProps}
						>
							<div className={styles.colHeading}>
								<div className='w-full flex justify-between gap-2'>
									<TransparentField
										{...register('name')}
										placeholder={'Enter group name'}
									/>
									<div className={styles.cardActions}>
										<button
											onClick={() =>
												value
													? deleteTaskColumn(value)
													: setColumns(prevState => prevState?.slice(0, -1))
											}
											className='opacity-50 transition-opacity hover:opacity-100'
										>
											{isDeletePending ? (
												<Loader size={15} />
											) : (
												<Trash size={15} />
											)}
										</button>
									</div>
									<button
										{...attributes}
										{...listeners}
										aria-describedby='list'
										className={styles.grip_btn_parent_list}
									>
										<GripVertical className={styles.grip_btn_list} />
									</button>
								</div>
							</div>

							{filterTasks(items, value, selectedTags)?.map((item, index) => (
								<Draggable
									draggableId={item.id}
									index={index}
									key={item.id}
								>
									{provided => (
										<div
											ref={provided.innerRef}
											{...provided.draggableProps}
											{...provided.dragHandleProps}
										>
											<ListRow
												selectedTags={selectedTags}
												setSelectedTags={setSelectedTags}
												item={item}
												setItems={setItems}
												key={item.id}
											/>
										</div>
									)}
								</Draggable>
							))}

							{provided.placeholder}

							{value !== 'completed' && !items?.some(item => !item.id) && (
								<ListAddRowInput
									columnId={value}
									setItems={setItems}
								/>
							)}
						</div>
					)}
				</Droppable>
			</div>
		</div>
	)
}
