'use client'

import { DndContext, closestCenter } from '@dnd-kit/core'
import { SortableContext, verticalListSortingStrategy } from '@dnd-kit/sortable'
import { DragDropContext } from '@hello-pangea/dnd'
import { Dispatch, SetStateAction } from 'react'

import { ITagResponse } from '@/types/tag.types'

import styles from './ListView.module.scss'
import { useGetTaskColumns } from '@/app/i/tasks/hooks/columns/useGetTaskColumns'
import { useTaskColumnDnd } from '@/app/i/tasks/hooks/columns/useTaskColumnDnd'
import { useTaskDnd } from '@/app/i/tasks/hooks/tasks/useTaskDnd'
import { useTasks } from '@/app/i/tasks/hooks/tasks/useTasks'
import ListAddColumn from '@/app/i/tasks/list-view/ListAddColumn'
import ListRowParent from '@/app/i/tasks/list-view/ListRowParent'

interface IListView {
	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function ListView({ setSelectedTags, selectedTags }: IListView) {
	const { items, setItems } = useTasks()
	const { columns, setColumns } = useGetTaskColumns()
	const { sensors, handleDragEnd } = useTaskColumnDnd(columns, setColumns)

	const { onDragEnd } = useTaskDnd()
	return (
		<DragDropContext onDragEnd={onDragEnd}>
			<div className={styles.table}>
				<div className={styles.header}>
					<div>Task name</div>
					<div>Description</div>
					<div>Priority</div>
					<div>Tags</div>
					<div></div>
				</div>
				<div className={styles.parentsWrapper}>
					<DndContext
						sensors={sensors}
						collisionDetection={closestCenter}
						onDragEnd={handleDragEnd}
					>
						<SortableContext
							items={columns || []}
							strategy={verticalListSortingStrategy}
						>
							<div className={styles.list}>
								<DragDropContext onDragEnd={onDragEnd}>
									{columns
										?.filter(column => {
											if (selectedTags.length === 0) {
												return true
											}
											const filteredItems = items?.filter(item =>
												selectedTags.every(tag =>
													item.tags?.some(itemTag => itemTag.id === tag.id)
												)
											)
											return filteredItems?.some(
												item => item.columnId === column.id
											)
										})
										?.map(column => (
											<ListRowParent
												setSelectedTags={setSelectedTags}
												selectedTags={selectedTags}
												value={column.id}
												label={column.name}
												items={items}
												setItems={setItems}
												setColumns={setColumns}
												key={column.id}
											/>
										))}
								</DragDropContext>
							</div>
						</SortableContext>
					</DndContext>
					{!columns?.some(column => !column.id) && (
						<ListAddColumn setColumns={setColumns} />
					)}
				</div>
			</div>
		</DragDropContext>
	)
}
