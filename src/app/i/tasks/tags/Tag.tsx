import clsx from 'clsx'
import { Dispatch, SetStateAction } from 'react'

import ContextMenu from '@/components/ui/context-menu/ContextMenu'

import { ITagResponse } from '@/types/tag.types'

import { useOutside } from '@/hooks/useOutside'

import styles from './Tag.module.scss'

interface ITag {
	itemId: string
	tagItem: ITagResponse
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
	selectedTags: Array<ITagResponse>
}

export default function Tag({
	tagItem,
	itemId,
	setSelectedTags,
	selectedTags
}: ITag) {
	const isSelectedTag = selectedTags.some(
		(tag: ITagResponse) => tag.id === tagItem.id
	)
	const { ref, isShow, setIsShow } = useOutside(false)
	const clickHandler = () => {
		console.log(itemId, tagItem)
		setSelectedTags(prev => {
			if (prev.some((tag: ITagResponse) => tag.id === tagItem.id)) {
				return prev.filter(tag => tag.id !== tagItem.id)
			} else {
				return [...prev, tagItem]
			}
		})
	}
	return (
		<div
			className={styles.tagContainer}
			title={'Click to select tag. Right click to delete tag.'}
			onContextMenu={e => {
				e.preventDefault()
				e.stopPropagation()
				setIsShow(prev => !prev)
			}}
		>
			<button
				className={clsx(styles.tag, isSelectedTag && styles.selectedTag)}
				onClick={clickHandler}
			>
				{tagItem.tag}
			</button>
			{isShow && (
				<ContextMenu
					setSelectedTags={setSelectedTags}
					itemId={itemId}
					ref={ref}
					tagId={tagItem.id}
					setIsShow={setIsShow}
				/>
			)}
		</div>
	)
}
