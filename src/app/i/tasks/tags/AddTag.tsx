import clsx from 'clsx'
import { PlusCircle as CirclePlus, Trash } from 'lucide-react'
import { useEffect, useRef, useState } from 'react'
import { toast } from 'sonner'

import Badge from '@/components/ui/Badge'
import Loader from '@/components/ui/Loader'

import {
	ITagToTaskResponse,
	TypeTagToTaskFormState
} from '@/types/tag-to-task.types'
import { TypeTagFormState } from '@/types/tag.types'

import { useClick } from '@/hooks/useClick'
import { useOutside } from '@/hooks/useOutside'

import styles from './Tag.module.scss'
import { useCreateTag } from '@/app/i/tasks/hooks/tags/useCreateTag'
import { useCreateTagToTask } from '@/app/i/tasks/hooks/tags/useCreateTagToTask'
import { useDeleteTag } from '@/app/i/tasks/hooks/tags/useDeleteTag'
import { useGetTags } from '@/app/i/tasks/hooks/tags/useGetTags'
import { useGetTagsToTask } from '@/app/i/tasks/hooks/tags/useGetTagsToTask'

interface IAddTag {
	taskId: string
	view: 'list' | 'kanban'
	usedTags?: Array<string>
}

export default function AddTag({ taskId, view, usedTags = [] }: IAddTag) {
	const { tags } = useGetTags()
	const [tag, setTag] = useState<TypeTagFormState>({ tag: '' })
	const [tagToTask, setTagToTask] = useState<TypeTagToTaskFormState>({
		taskId: taskId,
		tagId: ''
	})
	const [value, setValue] = useState('')
	const [isTagExist, setIsTagExist] = useState(false)

	const {
		isShow: isShowInput,
		ref: refInputParent,
		setIsShow: setIsShowInput
	} = useOutside(false)

	const { createTag, isCreateTagSuccess, newTags } = useCreateTag()
	const { createTagToTask } = useCreateTagToTask()
	const { handleClick } = useClick('Enter')
	const refInput = useRef<HTMLInputElement>(null)

	function addTag() {
		if (isShowInput) {
			if (isTagExist || tags?.data.find(item => item.tag === tag.tag)) {
				createTagToTask({ data: tagToTask })
			} else {
				// Предположим, что у вас есть значение тега, которое нужно создать
				createTag({ data: tag })
			}
			setValue('')
			setIsShowInput(false)
		} else {
			refInput.current?.focus()
			setIsShowInput(true)
		}
	}

	// Возвращаемое значение createTagPending
	// Используйте useEffect для выполнения createTagToTask после создания тега
	useEffect(() => {
		if (isCreateTagSuccess) {
			createTagToTask({
				data: {
					...tagToTask,
					// Предположим, что у вас есть массив тегов с уникальными id
					tagId: newTags?.data.find(tagData => tagData.tag === tag.tag)?.id
				}
			})
		}
	}, [isCreateTagSuccess])
	function changeSelect(value: string) {
		setValue(tags?.data.find(tag => tag.id === value)?.tag || '')
		setTagToTask({
			...tagToTask,
			tagId: value
		})
		setTag({ tag: tags?.data.find(tag => tag.id === value)?.tag })
		refInput.current?.focus()
	}

	useEffect(() => {
		if (tags?.data.find(item => item.tag === tag.tag)) {
			setIsTagExist(true)
		} else {
			setIsTagExist(false)
		}
	}, [tagToTask, tag])
	const { deleteTag, isDeletePending } = useDeleteTag()
	const { tagToTasks } = useGetTagsToTask()
	async function deleteTagHandler(tagId: string) {
		try {
			if (
				tagToTasks?.data.find(
					(tagToTask: ITagToTaskResponse) => tagToTask.tagId === tagId
				)
			) {
				toast('Tag is in use in the task.')
				return
			}
			await deleteTag(tagId)
			toast('Tag deleted successfully')
		} catch (error) {
			toast('Error deleting tag. Please try again later.')
		}
	}
	return (
		<div
			className={styles.add}
			data-view={view}
			data-isShowInput={isShowInput}
		>
			{isShowInput && (
				<div ref={refInputParent}>
					<input
						className={clsx(
							'bg-transparent border-none focus:outline-0 focus:shadow-transparent w-full',
							styles.input
						)}
						onKeyDown={e => handleClick(e, addTag)}
						value={value}
						onChange={e => {
							setValue(e.target.value)
							setTag({ tag: e.target.value })
							setTagToTask({
								...tagToTask,
								tagId:
									tags?.data.find(tag => tag.tag === e.target.value)?.id || ''
							})
						}}
						placeholder={'Click Enter to add a tag'}
						title={'Click Enter to add a tag'}
						ref={refInput}
					/>
					{tags &&
						tags?.data
							.map(tag => ({ label: tag.tag, value: tag.id }))
							.filter(tag => tag.label.toString().toLowerCase().includes(value))
							.length > 0 && (
							<>
								<div
									className={clsx(
										'absolute w-full p-3.5 left-0 slide bg-sidebar z-[100] shadow rounded-lg overflow-x-hidden box-border',
										styles.selectContainer
									)}
									style={{
										top: 'calc(100% + .5rem)'
									}}
								>
									{tags?.data
										.map(tag => ({ label: tag.tag, value: tag.id }))
										.filter(tag => !usedTags?.includes(tag.value))
										.filter(tag =>
											tag.label.toString().toLowerCase().includes(value)
										)
										.map(item => {
											if (!item.label.toLowerCase().includes(value)) {
												return null
											}
											return (
												<div className={clsx('w-full', styles.select)}>
													<div className={'w-full overflow-hidden'}>
														<button
															key={item.value}
															onClick={e => {
																e.preventDefault()
																changeSelect(item.value)
															}}
															className='block rounded-lg text-left w-full '
															onContextMenu={e => {
																e.preventDefault()
															}}
														>
															<Badge variant={item.value}>{item.label}</Badge>
														</button>
													</div>

													<div>
														<button
															onClick={() => deleteTagHandler(item.value)}
															className='opacity-50 transition-opacity hover:opacity-100'
														>
															{isDeletePending ? (
																<Loader size={14} />
															) : (
																<Trash size={14} />
															)}
														</button>
													</div>
												</div>
											)
										})}
								</div>
							</>
						)}
				</div>
			)}
			<button onClick={addTag}>
				<CirclePlus />
			</button>
		</div>
	)
}
