import { Dispatch, SetStateAction } from 'react'

import type { ITagResponse } from '@/types/tag.types'

import { useHorizontalMouseParallax } from '@/hooks/useHorizontalMouseParallax'

import styles from './Tag.module.scss'
import Tag from '@/app/i/tasks/tags/Tag'

interface ITags {
	itemId: string
	tags: Array<ITagResponse> | []
	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function Tags({
	tags,
	itemId,
	setSelectedTags,
	selectedTags
}: ITags) {
	const { ref } = useHorizontalMouseParallax()
	return (
		<div
			ref={ref}
			className={styles.tags}
		>
			{tags.map(tag => (
				<Tag
					selectedTags={selectedTags}
					setSelectedTags={setSelectedTags}
					key={tag.id}
					tagItem={tag}
					itemId={itemId}
				/>
			))}
		</div>
	)
}
