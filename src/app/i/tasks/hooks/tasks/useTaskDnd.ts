import { DropResult } from '@hello-pangea/dnd'
import { useUpdateTask } from '@/app/i/tasks/hooks/tasks/useUpdateTask'
import {useGetTaskColumns} from "@/app/i/tasks/hooks/columns/useGetTaskColumns";

export function useTaskDnd() {
	const {columns} = useGetTaskColumns()
	const { updateTask } = useUpdateTask()

	const onDragEnd = (result: DropResult) => {
		if (!result.destination) return

		const destinationColumnId = result.destination.droppableId

		if (!columns) return
		if (destinationColumnId === result.source.droppableId) return

		console.log('destinationColumnId', destinationColumnId)
		if (destinationColumnId === columns[columns.length - 1].id) {
			console.log('last column')
			updateTask({
				id: result.draggableId,
				data: {
					isCompleted: true,
					columnId: destinationColumnId
				}
			})
			return
		}


		updateTask({
			id: result.draggableId,
			data: {
				// isCompleted: false,
				columnId: destinationColumnId
			}
		})
	}

	return { onDragEnd }
}
