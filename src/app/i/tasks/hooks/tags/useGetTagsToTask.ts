import { useQuery } from '@tanstack/react-query'

import { tagToTaskService } from '@/services/tag-to-task.service'

export function useGetTagsToTask() {
	const { data: tagToTasks } = useQuery({
		queryKey: ['tag to task'],
		queryFn: () => tagToTaskService.getTagsToTask()
	})

	return { tagToTasks }
}
