import { useMutation, useQueryClient } from '@tanstack/react-query'

import { TypeTagToTaskFormState } from '@/types/tag-to-task.types'

import { tagToTaskService } from '@/services/tag-to-task.service'

export function useDeleteTagToTask() {
	const queryClient = useQueryClient()

	const { mutate: deleteTagToTask, isPending: isDeletePending } = useMutation({
		mutationKey: ['delete tag to task'],
		mutationFn: (tagToTask: TypeTagToTaskFormState) =>
			tagToTaskService.deleteTagToTask(tagToTask),
		onSuccess: () => {
			queryClient.invalidateQueries({
				queryKey: ['tasks']
			})
			queryClient.invalidateQueries({
				queryKey: ['tags']
			})
			queryClient.invalidateQueries({
				queryKey: ['tag to task']
			})
		}
	})

	return { deleteTagToTask, isDeletePending }
}
