import { useQuery } from '@tanstack/react-query'

import { tagService } from '@/services/tag.service'

export function useGetTagByName(name: string) {
	const { data: tag } = useQuery({
		queryKey: ['tag', name],
		queryFn: () => tagService.getTagByName(name)
	})

	return { tag }
}
