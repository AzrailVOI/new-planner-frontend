import { useMutation, useQueryClient } from '@tanstack/react-query'

import { TypeTagFormState } from '@/types/tag.types'

import { tagService } from '@/services/tag.service'

export function useCreateTag() {
	const queryClient = useQueryClient()

	const {
		mutate: createTag,
		isSuccess: isCreateTagSuccess,
		data: newTags
	} = useMutation({
		mutationKey: ['tags'],
		mutationFn: ({ data }: { data: TypeTagFormState }) =>
			tagService.createTag(data),
		onSuccess: () => {
			queryClient.invalidateQueries({
				queryKey: ['tags']
			})
			queryClient.invalidateQueries({
				queryKey: ['tag to task']
			})
		}
	})

	return { createTag, isCreateTagSuccess, newTags }
}
