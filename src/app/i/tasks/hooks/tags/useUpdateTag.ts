import { useMutation, useQueryClient } from '@tanstack/react-query'

import { TypeTagFormState } from '@/types/tag.types'

import { tagService } from '@/services/tag.service'

export function useUpdateTag() {
	const queryClient = useQueryClient()

	const { mutate: updateTag } = useMutation({
		mutationKey: ['update task'],
		mutationFn: ({ id, data }: { id: string; data: TypeTagFormState }) =>
			tagService.updateTag(id, data),
		onSuccess: () => {
			queryClient.invalidateQueries({ queryKey: ['tasks', 'tags'] })
		}
	})

	return { updateTag }
}
