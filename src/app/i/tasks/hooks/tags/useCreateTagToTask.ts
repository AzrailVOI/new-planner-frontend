import { useMutation, useQueryClient } from '@tanstack/react-query'

import { TypeTagToTaskFormState } from '@/types/tag-to-task.types'

import { tagToTaskService } from '@/services/tag-to-task.service'

export function useCreateTagToTask() {
	const queryClient = useQueryClient()

	const { mutate: createTagToTask } = useMutation({
		mutationKey: ['create tag to task'],
		mutationFn: ({ data }: { data: TypeTagToTaskFormState }) =>
			tagToTaskService.createTagToTask(data),
		onSuccess: () => {
			queryClient.invalidateQueries({
				queryKey: ['tasks']
			})
			queryClient.invalidateQueries({
				queryKey: ['tag to task']
			})
		}
	})

	return { createTagToTask }
}
