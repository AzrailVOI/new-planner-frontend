import { useQuery } from '@tanstack/react-query'

import { tagToTaskService } from '@/services/tag-to-task.service'

export function useGetTagToTaskByTag(tagId: string) {
	const { data: tagToTaskByTag } = useQuery({
		queryKey: ['tag to task'],
		queryFn: () => tagToTaskService.getTagsToTaskByTag(tagId)
	})

	return { tagToTaskByTag }
}
