import { useQuery } from '@tanstack/react-query'

import { tagService } from '@/services/tag.service'

export function useGetTags() {
	const { data: tags } = useQuery({
		queryKey: ['tags'],
		queryFn: () => tagService.getTags()
	})

	return { tags }
}
