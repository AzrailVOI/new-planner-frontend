import { useMutation, useQueryClient } from '@tanstack/react-query'

import { TypeTaskColumnFormState } from '@/types/task-column.types'

import { taskColumnService } from '@/services/task-column.service'

export function useCreateTaskColumn() {
	const queryClient = useQueryClient()

	const { mutate: createTaskColumn } = useMutation({
		mutationKey: ['create task column'],
		mutationFn: ({ data }: { data: TypeTaskColumnFormState }) =>
			taskColumnService.createTaskColumn(data),
		onSuccess: () => {
			queryClient.invalidateQueries({ queryKey: ['task-columns'] })
		}
	})

	return { createTaskColumn }
}
