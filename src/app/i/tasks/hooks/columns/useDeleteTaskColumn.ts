import { useMutation, useQueryClient } from '@tanstack/react-query'

import { taskColumnService } from '@/services/task-column.service'

export function useDeleteTaskColumn() {
	const queryClient = useQueryClient()

	const { mutate: deleteTaskColumn, isPending: isDeletePending } = useMutation({
		mutationKey: ['delete task column'],
		mutationFn: (id: string) => taskColumnService.deleteTaskColumn(id),
		onSuccess: () => {
			queryClient.invalidateQueries({
				queryKey: ['task-columns']
			})
		}
	})

	return { deleteTaskColumn, isDeletePending }
}
