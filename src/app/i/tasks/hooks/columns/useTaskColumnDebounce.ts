import { debounce } from 'next/dist/server/utils'
import { useCallback, useEffect } from 'react'
import { UseFormWatch } from 'react-hook-form'

import { TypeTaskColumnFormState } from '@/types/task-column.types'

import { useCreateTaskColumn } from '@/app/i/tasks/hooks/columns/useCreateTaskColumn'
import { useUpdateTaskColumn } from '@/app/i/tasks/hooks/columns/useUpdateTaskColumn'

interface IUseTaskColumnDebounce {
	watch: UseFormWatch<TypeTaskColumnFormState>
	itemId: string
}

export function useTaskColumnDebounce({
	watch,
	itemId
}: IUseTaskColumnDebounce) {
	const { updateTaskColumn } = useUpdateTaskColumn()
	const { createTaskColumn } = useCreateTaskColumn()

	const debounceCreateTaskColumn = useCallback(
		debounce((formData: TypeTaskColumnFormState) => {
			createTaskColumn({ data: formData })
		}, 500),
		[]
	)

	const debounceUpdateTaskColumn = useCallback(
		debounce((formData: TypeTaskColumnFormState) => {
			updateTaskColumn({ id: itemId, data: formData })
		}, 1000),
		[]
	)

	useEffect(() => {
		const { unsubscribe } = watch(formData => {
			if (itemId) {
				debounceUpdateTaskColumn({
					...formData,
					order: formData.order
				})
			} else {
				debounceCreateTaskColumn(formData)
			}
		})

		return () => {
			unsubscribe()
		}
	}, [watch(), debounceUpdateTaskColumn, debounceCreateTaskColumn])
}
