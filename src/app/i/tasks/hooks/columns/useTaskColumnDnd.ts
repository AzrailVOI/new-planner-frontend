import {
	DragEndEvent,
	KeyboardSensor,
	PointerSensor,
	useSensor,
	useSensors
} from '@dnd-kit/core'
import { arrayMove } from '@dnd-kit/sortable'
import { useMutation, useQueryClient } from '@tanstack/react-query'
import { Dispatch, SetStateAction } from 'react'

import { ITaskColumnResponse } from '@/types/task-column.types'

import { taskColumnService } from '@/services/task-column.service'

export function useTaskColumnDnd(
	items: ITaskColumnResponse[] | undefined,
	setItems: Dispatch<SetStateAction<ITaskColumnResponse[] | undefined>>
) {
	const sensors = useSensors(
		useSensor(PointerSensor),
		useSensor(KeyboardSensor)
	)
	const queryClient = useQueryClient()
	const { mutate } = useMutation({
		mutationKey: ['update order task column'],
		mutationFn: (ids: string[]) => taskColumnService.updateOrder(ids),
		onSuccess: () => {
			queryClient.invalidateQueries({ queryKey: ['task-columns'] })
		}
	})
	const handleDragEnd = (event: DragEndEvent) => {
		const { active, over } = event

		if (active.id !== over?.id && items) {
			const oldIndex = items.findIndex(item => item.id === active.id)
			const newIndex = items.findIndex(item => item.id === (over?.id || ''))

			if (oldIndex !== -1 && newIndex !== -1) {
				// Создаем новый отсортированный массив
				const newItems = arrayMove(items, oldIndex, newIndex)
				// Обновляем состояние
				setItems(newItems)
				// Обновляем порядок на сервере
				mutate(newItems.map(item => item.id))
			}
		}
	}
	return { handleDragEnd, sensors }
}
