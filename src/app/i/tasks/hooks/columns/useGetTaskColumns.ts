import {useQuery} from "@tanstack/react-query";
import {taskColumnService} from "@/services/task-column.service";
import {useEffect, useState} from "react";
import {ITaskColumnResponse} from "@/types/task-column.types";

export function useGetTaskColumns() {

    const { data } = useQuery({
		queryKey: ['task-columns'],
		queryFn: () => taskColumnService.getTaskColumns()
	})

	const [items, setItems] = useState<ITaskColumnResponse[] | undefined>(data?.data)

	useEffect(() => {
		if (!data?.data) return
		setItems(data.data)
	}, [data?.data])

	return { columns: items, setColumns: setItems }
}
