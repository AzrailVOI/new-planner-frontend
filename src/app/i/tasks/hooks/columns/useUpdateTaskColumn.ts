import { useMutation, useQueryClient } from '@tanstack/react-query'
import {TypeTaskColumnFormState} from "@/types/task-column.types";
import {taskColumnService} from "@/services/task-column.service";


export function useUpdateTaskColumn() {
	const queryClient = useQueryClient()

	const { mutate: updateTaskColumn } = useMutation({
		mutationKey: ['update task column'],
		mutationFn: ({ id, data }: { id: string; data: TypeTaskColumnFormState }) =>
			taskColumnService.updateTaskColumn(id, data),
		onSuccess: () => {
			queryClient.invalidateQueries({ queryKey: ['task-columns'] })
		}
	})

	return { updateTaskColumn }
}
