import { Draggable, Droppable } from '@hello-pangea/dnd'
import { GripVertical, Trash } from 'lucide-react'
import { Dispatch, SetStateAction } from 'react'
import { useForm } from 'react-hook-form'

import Loader from '@/components/ui/Loader'
import { TransparentField } from '@/components/ui/fields/TransparentField'

import { ITagResponse } from '@/types/tag.types'
import {
	ITaskColumnResponse,
	TypeTaskColumnFormState
} from '@/types/task-column.types'
import { ITaskResponse } from '@/types/task.types'

import styles from './KanbanView.module.scss'
import { filterTasks } from '@/app/i/tasks/filter-tasks'
import { useDeleteTaskColumn } from '@/app/i/tasks/hooks/columns/useDeleteTaskColumn'
import { useTaskColumnDebounce } from '@/app/i/tasks/hooks/columns/useTaskColumnDebounce'
import { useTaskColumnSortable } from '@/app/i/tasks/hooks/columns/useTaskColumnSortable'
import KanbanAddCardInput from '@/app/i/tasks/kanban-view/KanbanAddCardInput'
import KanbanCard from '@/app/i/tasks/kanban-view/KanbanCard'

interface IKanbanColumn {
	value: string
	label: string
	items: ITaskResponse[] | undefined
	setColumns: Dispatch<SetStateAction<ITaskColumnResponse[] | undefined>>
	setItems: Dispatch<SetStateAction<ITaskResponse[] | undefined>>

	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function KanbanColumn({
	value,
	items,
	setItems,
	label,
	setColumns,
	setSelectedTags,
	selectedTags
}: IKanbanColumn) {
	const { register, watch } = useForm<TypeTaskColumnFormState>({
		defaultValues: {
			name: label
		}
	})
	useTaskColumnDebounce({ watch, itemId: value })

	const { deleteTaskColumn, isDeletePending } = useDeleteTaskColumn()

	const { attributes, style, setNodeRef, listeners } =
		useTaskColumnSortable(value)
	return (
		<div
			ref={setNodeRef}
			style={style}
		>
			<div className={styles.block}>
				<Droppable droppableId={value}>
					{provided => (
						<div
							ref={provided.innerRef}
							{...provided.droppableProps}
						>
							<div className={styles.column}>
								<div className={styles.columnHeading}>
									<div className='w-full flex justify-between gap-1.5'>
										<TransparentField
											{...register('name')}
											placeholder={'Enter column name'}
										/>
										<div className={styles.cardActions}>
											<button
												onClick={() =>
													value
														? deleteTaskColumn(value)
														: setColumns(prevState => prevState?.slice(0, -1))
												}
												className='opacity-50 transition-opacity hover:opacity-100'
											>
												{isDeletePending ? (
													<Loader size={15} />
												) : (
													<Trash size={15} />
												)}
											</button>
										</div>
										<button
											{...attributes}
											{...listeners}
											aria-describedby='column-column'
											className={styles.grip_btn_parent}
										>
											<GripVertical className={styles.grip_btn} />
										</button>
									</div>
								</div>

								{filterTasks(items, value, selectedTags)?.map((item, index) => (
									<Draggable
										key={item.id}
										draggableId={item.id}
										index={index}
									>
										{provided => (
											<div
												ref={provided.innerRef}
												{...provided.draggableProps}
												{...provided.dragHandleProps}
											>
												<KanbanCard
													selectedTags={selectedTags}
													key={item.id}
													item={item}
													setItems={setItems}
													setSelectedTags={setSelectedTags}
												/>
											</div>
										)}
									</Draggable>
								))}

								{provided.placeholder}

								{value !== 'completed' && !items?.some(item => !item.id) && (
									<KanbanAddCardInput
										setItems={setItems}
										columnId={value}
									/>
								)}
							</div>
						</div>
					)}
				</Droppable>
			</div>
		</div>
	)
}
