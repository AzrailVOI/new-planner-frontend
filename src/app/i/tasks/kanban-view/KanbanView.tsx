'use client'

import { DndContext, closestCenter } from '@dnd-kit/core'
import {
	SortableContext,
	horizontalListSortingStrategy
} from '@dnd-kit/sortable'
import { DragDropContext } from '@hello-pangea/dnd'
import { Dispatch, SetStateAction } from 'react'

import { ITagResponse } from '@/types/tag.types'
import { ITaskColumnResponse } from '@/types/task-column.types'

import styles from './KanbanView.module.scss'
import { useGetTaskColumns } from '@/app/i/tasks/hooks/columns/useGetTaskColumns'
import { useTaskColumnDnd } from '@/app/i/tasks/hooks/columns/useTaskColumnDnd'
import { useTaskDnd } from '@/app/i/tasks/hooks/tasks/useTaskDnd'
import { useTasks } from '@/app/i/tasks/hooks/tasks/useTasks'
import KanbanAddColumn from '@/app/i/tasks/kanban-view/KanbanAddColumn'
import KanbanColumn from '@/app/i/tasks/kanban-view/KanbanColumn'

interface IKanbanView {
	selectedTags: Array<ITagResponse>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}

export default function KanbanView({
	setSelectedTags,
	selectedTags
}: IKanbanView) {
	const { items, setItems } = useTasks()
	const { columns, setColumns } = useGetTaskColumns()
	const { sensors, handleDragEnd } = useTaskColumnDnd(columns, setColumns)
	const { onDragEnd } = useTaskDnd()
	return (
		<div className={styles.board}>
			<DndContext
				sensors={sensors}
				collisionDetection={closestCenter}
				onDragEnd={handleDragEnd}
			>
				<SortableContext
					items={columns || []}
					strategy={horizontalListSortingStrategy}
				>
					<div className={styles.columns}>
						<DragDropContext onDragEnd={onDragEnd}>
							{columns
								?.filter(column => {
									if (selectedTags.length === 0) {
										return true
									}
									const filteredItems = items?.filter(item =>
										selectedTags.every(tag =>
											item.tags?.some(itemTag => itemTag.id === tag.id)
										)
									)
									return filteredItems?.some(
										item => item.columnId === column.id
									)
								})
								?.map((column: ITaskColumnResponse) => (
									<KanbanColumn
										key={column.id}
										value={column.id}
										label={column.name}
										items={items}
										setItems={setItems}
										setColumns={setColumns}
										selectedTags={selectedTags}
										setSelectedTags={setSelectedTags}
									/>
								))}
						</DragDropContext>
					</div>
				</SortableContext>
			</DndContext>
			{!columns?.some(column => !column.id) && (
				<KanbanAddColumn setColumns={setColumns} />
			)}
		</div>
	)
}
