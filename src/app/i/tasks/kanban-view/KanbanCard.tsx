import clsx from 'clsx'
import { GripVertical, Trash } from 'lucide-react'
import type { Dispatch, SetStateAction } from 'react'
import { Controller, useForm } from 'react-hook-form'

import Loader from '@/components/ui/Loader'
import Checkbox from '@/components/ui/checkbox'
import { TransparentField } from '@/components/ui/fields/TransparentField'
import SingleSelect from '@/components/ui/task-edit/SingleSelect'

import { ITagResponse } from '@/types/tag.types'
import {
	type ITaskResponse,
	type TypeTaskFormState,
	priorityList
} from '@/types/task.types'

import styles from './KanbanView.module.scss'
import { useDeleteTask } from '@/app/i/tasks/hooks/tasks/useDeleteTask'
import { useTaskDebounce } from '@/app/i/tasks/hooks/tasks/useTaskDebounce'
import AddTag from '@/app/i/tasks/tags/AddTag'
import Tags from '@/app/i/tasks/tags/Tags'

interface IKanbanCard {
	item: ITaskResponse
	setItems: Dispatch<SetStateAction<ITaskResponse[] | undefined>>
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
	selectedTags: Array<ITagResponse>
}

export default function KanbanCard({
	item,
	setItems,
	setSelectedTags,
	selectedTags
}: IKanbanCard) {
	const { register, control, watch } = useForm<TypeTaskFormState>({
		defaultValues: {
			name: item.name,
			isCompleted: item.isCompleted,
			priority: item.priority,
			columnId: item.columnId,
			description: item.description
		}
	})

	useTaskDebounce({ watch, itemId: item.id })

	const { deleteTask, isDeletePending } = useDeleteTask()

	return (
		<div
			className={clsx(
				styles.card,
				watch('isCompleted') ? styles.completed : '',
				'animation-opacity'
			)}
		>
			<div className={styles.cardHeader}>
				<button
					aria-describedby='todo-item'
					className={styles.grip_card_parent}
				>
					<GripVertical className={styles.grip_card} />
				</button>

				<Controller
					name='isCompleted'
					control={control}
					render={({ field: { value, onChange } }) => (
						<Checkbox
							checked={value}
							onChange={onChange}
						/>
					)}
				/>

				<TransparentField
					{...register('name')}
					placeholder={'Enter task name'}
				/>
			</div>

			<div className={styles.cardBody}>
				<TransparentField
					{...register('description')}
					placeholder={'Enter task description'}
				/>

				<Controller
					control={control}
					name='priority'
					render={({ field: { value, onChange } }) => (
						<SingleSelect
							data={priorityList.map(item => ({
								value: item,
								label: item
							}))}
							onChange={onChange}
							value={value || ''}
						/>
					)}
				/>
				<div
					style={{ width: '100%', display: 'flex', justifyContent: 'center' }}
				>
					{item.tags && (
						<div className={styles.tags_container}>
							<Tags
								selectedTags={selectedTags}
								setSelectedTags={setSelectedTags}
								tags={item.tags}
								itemId={item.id}
							/>
							<div>
								<AddTag
									taskId={item.id}
									view={'kanban'}
									usedTags={item.tags.map((tag: ITagResponse) => tag.id)}
								/>
							</div>
						</div>
					)}
				</div>
			</div>

			<div className={styles.cardActions}>
				<button
					onClick={() =>
						item.id
							? deleteTask(item.id)
							: setItems(prevState => prevState?.slice(0, -1))
					}
					className='opacity-50 transition-opacity hover:opacity-100'
				>
					{isDeletePending ? <Loader size={15} /> : <Trash size={15} />}
				</button>
			</div>

			<div className={styles.cardBody}></div>
		</div>
	)
}
