import type { IBase } from '@/types/root.types'

export interface ITagResponse extends IBase {
	tag: string
}

export type TypeTagFormState = Partial<Omit<ITagResponse, 'id'>>
