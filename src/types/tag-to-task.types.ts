export interface ITagToTaskResponse {
	tagId: string
	taskId: string
}

export type TypeTagToTaskFormState = Partial<ITagToTaskResponse>
