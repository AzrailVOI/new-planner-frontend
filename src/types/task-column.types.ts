import type { IBase } from '@/types/root.types'

export enum EnumTaskPriority {
	low = 'low',
	medium = 'medium',
	high = 'high'
}
export const priorityList: Array<keyof typeof EnumTaskPriority> = [
	'low',
	'medium',
	'high'
]
export interface ITaskColumnResponse extends IBase {
	name: string
	order: number
}

export type TypeTaskColumnFormState = Partial<Omit<ITaskColumnResponse, 'id'>>
