import type { IBase } from '@/types/root.types'
import { ITagResponse } from '@/types/tag.types'

export enum EnumTaskPriority {
	low = 'low',
	medium = 'medium',
	high = 'high'
}
export const priorityList: Array<keyof typeof EnumTaskPriority> = [
	'low',
	'medium',
	'high'
]
export interface ITaskResponse extends IBase {
	name: string
	priority?: EnumTaskPriority
	isCompleted: boolean
	description?: string
	columnId: string
	tags?: Array<ITagResponse> | []
}

export type TypeTaskFormState = Partial<Omit<Omit<ITaskResponse, 'id'>, 'tags'>>
