import type { PropsWithChildren } from 'react'

import DashboardHeader from '@/components/dashboard-layout/header/DashboardHeader'
import DashboardSidebar from '@/components/dashboard-layout/sidebar/DashboardSidebar'

export default function DashboardLayout({
	children
}: PropsWithChildren<unknown>) {
	return (
		<div className='grid min-h-screen 2xl:grid-cols-[1.1fr_6fr] grid-cols-[1.2fr_6fr]'>
			<DashboardSidebar />

			<main
				className='px-big-layout pt-big-layout overflow-x-hidden relative'
				style={{ maxHeight: '100dvh' }}
			>
				<DashboardHeader />
				{children}
			</main>
		</div>
	)
}
