import { Trash } from 'lucide-react'
import { Dispatch, ForwardedRef, SetStateAction, forwardRef } from 'react'

import { ITagResponse } from '@/types/tag.types'

import styles from './ContextMenu.module.scss'
import { useDeleteTagToTask } from '@/app/i/tasks/hooks/tags/useDeleteTagToTask'

interface IContextMenu {
	tagId: string
	setIsShow: Dispatch<SetStateAction<boolean>>
	itemId: string
	setSelectedTags: Dispatch<SetStateAction<Array<ITagResponse>>>
}
const ContextMenu = forwardRef(
	(
		{ setIsShow, tagId, itemId, setSelectedTags }: IContextMenu,
		ref: ForwardedRef<HTMLDivElement>
	) => {
		const { deleteTagToTask } = useDeleteTagToTask()

		const clickHandler = () => {
			deleteTagToTask({ taskId: itemId, tagId })
			setSelectedTags(prevState => prevState.filter(tag => tag.id !== tagId))
			setIsShow(false)
		}
		return (
			<div
				className={styles.menu}
				ref={ref}
				onClick={clickHandler}
				onContextMenu={e => {
					e.preventDefault()
					setIsShow(prevState => !prevState)
				}}
			>
				<button
					className={styles.menuItem}
					onContextMenu={e => {
						e.preventDefault()
						setIsShow(prevState => !prevState)
					}}
				>
					<Trash />
				</button>
			</div>
		)
	}
)
export default ContextMenu
